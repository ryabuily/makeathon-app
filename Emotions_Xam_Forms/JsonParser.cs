﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Emotions_Xam_Forms
{
    public class JsonParser
    {
        public JsonParser()
        {
        }

        public RecognizedHuman SetHuman(string json)
        {
            var arr = JArray.Parse(json);
            RecognizedHuman res = new RecognizedHuman();
            foreach (var obj in arr)
            {
                //var obj = JObject.Parse(json);
                JToken attributes = (obj["faceAttributes"]);
                res.Age = (double)attributes["age"];
                res.Gender = (string)attributes["gender"];
                res.Smile = (double)attributes["smile"];
                JToken emotions = attributes["emotion"];
                res.Emotions.Add("anger", (double)emotions["anger"]);
                res.Emotions.Add("contempt", (double)emotions["contempt"]);
                res.Emotions.Add("disgust", (double)emotions["disgust"]);
                res.Emotions.Add("fear", (double)emotions["fear"]);
                res.Emotions.Add("happiness", (double)emotions["happiness"]);
                res.Emotions.Add("neutral", (double)emotions["neutral"]);
                res.Emotions.Add("sadness", (double)emotions["sadness"]);
                res.Emotions.Add("surprise", (double)emotions["surprise"]);
            }
            return res;
        }
        /*
        public Dictionary<string, double> GetMaxEmotions(RecognizedHuman human)
        {
            var res = new Dictionary<string, double>();
            int len = 0;
            var list = human.Emotions;
            foreach (var item in list.OrderBy(x => x.Value).Reverse())
            {
                if (++len <= 3)
                    res.Add(item.Key, item.Value);
                else return res;

            }
            return res;
        }
        */

    }
}
