﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Emotions_Xam_Forms
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            Title = "Depression Tester";
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            switch (App.CurrentStage)
            {
                case App.DepressionStages.None:
                    txt_Depression.Text = "";
                    break;
                case App.DepressionStages.Healthy:
                    txt_Depression.Text = "You're healthy, just relax :)";
                    break;
                case App.DepressionStages.NearDepression:
                    txt_Depression.Text = "You're near depression. Please take more care of yourself";
                    break;
                case App.DepressionStages.MiddleDepression:
                    txt_Depression.Text = "You're usually depressing. Please visit some clinic in the nearest future.";
                    break;
                case App.DepressionStages.HardDepression:
                    txt_Depression.Text = "You're in depression. Please visit clinic as soon as possible!";
                    break;
            }
            if (App.Result > 1e-30)
                txt_res.Text = "Depression probability: " + Math.Round(100 - App.Result, 2) + "%";

        }




        void Handle_Clicked(object sender, System.EventArgs e)
        {
            JsonParser parser = new JsonParser();
            var res = parser.SetHuman(TakePhoto.TakeAsync().Result).Emotions;

            List<double> koeffs = new List<double>() { -0.054357507285721966, -0.0457301373200163, 0.0006234183939896151, 0.004777775036546332, -0.4693284399533533, 0.20601885613446197, 0.5852696877980534, -0.22941238910424033, 0.4408029956235689 };
            int indx = 0;
            double depressed = koeffs[-1];
            foreach (var item in res)
            {
                depressed += item.Value * koeffs[indx];
                indx += 1;
            }
            if (depressed > 1)
                depressed = 1.0;
            else if (depressed < 0)
                depressed = 0.0;
            depressed *= 100;
            res.Add("Probability of being depressed", depressed);
            txt_res.Text = printDictionary(res);
        }

        async void btn_GoToTestClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new QATestPage());
        }

        async void GoToPicTest(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new PictureTest());
        }

        string printDictionary(Dictionary<string, double> list)
        {
            string res = string.Empty;
            foreach(var item in list)
            {
                res += item.Key + ": " + item.Value;
                res += '\n';
            }
            return res;
        }
    }
}
