﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Emotions_Xam_Forms
{
    public class PictureTest : ContentPage
    {
        public enum Emotions
        {
            None, Anger, Fear, Hapiness, Sadness, Surprise
        }
        public Dictionary<Emotions, double> NormalResults { get; set; } = new Dictionary<Emotions, double>();
        public Dictionary<Emotions, double> CurrentResults { get; set; } = new Dictionary<Emotions, double>();
        public Dictionary<Tuple<Emotions,int>, bool> isSeen { get; set; } = new Dictionary<Tuple<Emotions, int>, bool>(); 
        int PicNumber { get; set; } = 5;
        public PictureTest()
        {
            setNormalResults();
            setSeen();
            Change(PicNumber);


        }

        public async void Change(int num)
        {
            Picker pk = new Picker();
            if (num > 0)
            {

                StackLayout sl = GenerateNewImage();

                pk = setPickerSourse();
                sl.Children.Add(pk);
                Content = sl;

            }
            else
            {

                await Navigation.PopAsync();
            }
            pk.SelectedIndexChanged += (sender, e) =>
            {

                Change(--PicNumber);

            };
        }


        public StackLayout GenerateNewImage()
        {
            StackLayout sl = new StackLayout();
            Emotions em;
            int num;
            do
            {
                em = selectRandomEmotion();
                num = new Random().Next(1, 3);
            }
            while (isSeen[new Tuple<Emotions, int>(em, num)]);
            string path = setPath(em, num);
            Image img = new Image()
            {
                Source = path,
                WidthRequest = 300,
                HeightRequest = 300
            };
            isSeen[new Tuple<Emotions, int>(em, num)] = true;
            sl.Children.Add(img);
            return sl;
        }

        public Picker setPickerSourse()
        {
            Picker pk = new Picker();
            pk.Items.Add("Anger");
            pk.Items.Add("Fear");
            pk.Items.Add("Hapiness");
            pk.Items.Add("Sadness");
            pk.Items.Add("Surprise");
            return pk;
        }
        public Emotions selectRandomEmotion() {
            Emotions res = Emotions.None;
            int num = new Random().Next(1, 5);
            switch (num)
            {
                default:
                    break;
                case 1:
                    res = Emotions.Anger;
                    break;
                case 2:
                    res = Emotions.Fear;
                    break;
                case 3:
                    res = Emotions.Hapiness;
                    break;
                case 4:
                    res = Emotions.Sadness;
                    break;
                case 5:
                    res = Emotions.Surprise;
                    break;
            }
            return res;
        }
        public string setPath(Emotions em, int num)
        {
            string res = string.Empty;
            switch (em)
            {
                default:
                    break;
                case Emotions.Anger:
                    res = "anger_"+num+".png";
                    break;
                case Emotions.Fear:
                    res = "fear_" + num + ".png";
                    break;
                case Emotions.Hapiness:
                    res = "hapiness_" + num + ".png";
                    break;
                case Emotions.Sadness:
                    res = "sadness_" + num + ".png";
                    break;
                case Emotions.Surprise:
                    res = "surprise_" + num + ".png";
                    break;
            }
            return res;
        }
        public void setSeen()
        {
            foreach(var item in NormalResults)
            {
                for(int i =0; i< 3; ++i)
                {
                    isSeen.Add(new Tuple<Emotions, int>(item.Key, i), false);
                }
            }
        }
        public void setNormalResults()
        {
            NormalResults.Add(Emotions.Anger, 0.4);
            NormalResults.Add(Emotions.Fear, 0.5);
            NormalResults.Add(Emotions.Hapiness, 0.6);
            NormalResults.Add(Emotions.Sadness, 0.7);
            NormalResults.Add(Emotions.Surprise, 0.8);
        }
    }
}

