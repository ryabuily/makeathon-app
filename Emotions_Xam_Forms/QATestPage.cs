﻿using System;
using System.Collections.Generic;
using System.Timers;
using Xamarin.Forms;

namespace Emotions_Xam_Forms
{
    public class QATestPage : ContentPage
    {
        //public Timer timer { get; set; } = new Timer();
        public Dictionary<Slider, double> Answers { get; set; } = new Dictionary<Slider, double>();
        public QATestPage()
        {
            Title = "Q&A Test";
            StackLayout layout = new StackLayout();
            layout.Children.Add(addQuestion("Little interest or pleasure in doings things"));
            layout.Children.Add(addQuestion("Feeling down, depressed, or hopeless"));
            layout.Children.Add(addQuestion("Trouble falling or staying asleep, or sleeping too much"));
            layout.Children.Add(addQuestion("Feeling tired or having little energy"));
            layout.Children.Add(addQuestion("Poor appetite or overeating"));
            layout.Children.Add(addQuestion("Feeling bad about yourself — or that you are a failure or have let yourself or your family down"));
            layout.Children.Add(addQuestion("Trouble concentrating on thins, such as reading the newspaper or watching television"));
            layout.Children.Add(addQuestion("Moving or speaking so slowly that other people could have noticed. Or the opposite — being so fidgety or restless that you have been moving around a lot more than usual"));
            layout.Children.Add(addQuestion("Thoughts that you would be better off dead, or of hurting yourself"));
            Button submit = new Button
            {
                Text = "Submit",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button))
            };
            layout.Children.Add(submit);
            Content = new ScrollView
            {
                Content = layout
            };
            submit.Clicked += GoBack;

        }

        async void GoBack(object sender, System.EventArgs e)
        {
            double totalAnswer = 0;
            foreach(var item in Answers)
            {
                totalAnswer += item.Value;
            }
            totalAnswer /= Answers.Count;
            if(totalAnswer > 0.8)
            {
                App.CurrentStage = App.DepressionStages.Healthy;
            }
            else if(totalAnswer > 0.6)
            {
                App.CurrentStage = App.DepressionStages.NearDepression;
            }
            else if (totalAnswer > 0.5)
            {
                App.CurrentStage = App.DepressionStages.MiddleDepression;
            }
            else
            {
                App.CurrentStage = App.DepressionStages.HardDepression;
            }
            App.Result = totalAnswer;
            await Navigation.PopAsync();
        }

        public StackLayout addQuestion(string question)
        {
            StackLayout layout = new StackLayout()
            {
                Margin = new Thickness(50),
                HorizontalOptions = LayoutOptions.Center
            };
            Label text = new Label
            {
                Text = question,
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center
            };
            layout.Children.Add(text);
            Slider slider = new Slider
            {
                Minimum = 0,
                Maximum = 100,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                WidthRequest = 300
            };
            slider.ValueChanged += (sender, e) => Answers[slider] = slider.Value;
            layout.Children.Add(slider);
            return layout;
        }
    }
}

