﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Emotions_Xam_Forms
{
    public class RecognizedHuman
    {
        public double Smile { get; set; }
        public string Gender { get; set; }
        public double Age { get; set; }
        public Dictionary<string, double> Emotions { get; set; }
        public RecognizedHuman()
        {
            Emotions = new Dictionary<string, double>();
        }
    }
}
