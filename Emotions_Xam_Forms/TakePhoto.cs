﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using PropertyChanged;
using Xamarin.Forms;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace Emotions_Xam_Forms
{
    public static class TakePhoto
    {
        //ImageSource ImageSource { get; set; }

        public async static Task<string> TakeAsync()
        {
            //bool ResultIsVisible = false;
            //bool IndicatorIsRunning = true;
            //int ResultFontSize = 16;
            var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            if (cameraStatus != PermissionStatus.Granted)
            {
                var semaphore = new SemaphoreSlim(1, 1);
                semaphore.Wait();
                var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
                cameraStatus = results[Permission.Camera];
                semaphore.Release();
            }
            if (cameraStatus == PermissionStatus.Granted)
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    //exception
                    throw new Exception();
                }
                else
                {
                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        Directory = "SampleDirectory",
                        Name = "test.jpg"
                    });
                    if (file == null)
                        return String.Empty;

                    ImageSource ImageSource = ImageSource.FromFile(file.Path);
                    return await EmotionsClass.MakeAnalysisRequest(file.Path);
                }
            }
            return String.Empty;
        }
    }
}
